'use client';
import React, { useEffect } from 'react';
import { useState } from 'react';

type SaveFormProps = {
    formTitle: string;
    noteData?: {
        title: string;
        content: string;
    };
    onSave: (title: string, content: string) => void;
};

const NoteForm = (props: SaveFormProps) => {
    const { formTitle, noteData, onSave } = props;
    const [title, setTitle] = useState(noteData?.title ?? '');
    const [content, setContent] = useState(noteData?.content ?? '');

    useEffect(() => {
        if (noteData) {
            setTitle(noteData.title);
            setContent(noteData.content);
        }
    }, [noteData]);

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        onSave(title, content);
    };

    const handleTitleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setTitle(e.target.value);
    };

    const handleContentChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setContent(e.target.value);
    };

    return (
        <div className="w-full flex flex-col items-center min-h-screen pt-12">
            <h1 className="text-3xl font-bold mb-4">{formTitle}</h1>
            <form onSubmit={handleSubmit} className="w-1/2">
                <div className="mb-4">
                    <label htmlFor="title" className="block font-bold mb-2">Title:</label>
                    <input
                        type="text"
                        id="title"
                        value={title}
                        onChange={handleTitleChange}
                        className="w-full border border-gray-300 rounded py-2 px-4 text-black"
                    />
                </div>
                <div className="mb-4">
                    <label htmlFor="content" className="block font-bold mb-2">Content:</label>
                    <textarea
                        id="content"
                        value={content}
                        onChange={handleContentChange}
                        className="w-full border border-gray-300 rounded py-2 px-4 h-40 text-black"
                    ></textarea>
                </div>
                <button type="submit" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" disabled={!title}>
                    Save
                </button>
            </form>
        </div>
    );
};


export default NoteForm;