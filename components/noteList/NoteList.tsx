'use client';
import { API_URL_BASE } from '@/utils/constants';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import DeleteConfirmModal from '../modals/DeleleConfirmModal';

interface Note {
    id: number;
    title: string;
    content: string;
    created_at: string;
    updated_at: string;
}

const NoteList: React.FC = () => {
    const [notes, setNotes] = useState<Note[]>([]);
    const [loading, setLoading] = React.useState<boolean>(true);
    const [noteIdToDelete, setNoteIdToDelete] = useState<number | null>(null);
    const [openConfirmModal, setOpenConfirmModal] = useState<boolean>(false);

    useEffect(() => {
        setLoading(true);
        fetch(`${API_URL_BASE}/notes`, { method: 'GET' })
            .then(response => response.json())
            .then(({ data }) => {
                setNotes(data ?? [])
            })
            .finally(() => setLoading(false));
    }, []);

    const handleOpenConfirmModal = (noteId: number) => () => {
        setOpenConfirmModal(true);
        setNoteIdToDelete(noteId);
    }

    const handleDelete = async () => {
        if (noteIdToDelete) {
            const response = await fetch(`${API_URL_BASE}/notes/${noteIdToDelete}`, {
                method: 'DELETE',
            });

            if (response.ok) {
                const updatedNotes = notes.filter((note: Note) => note.id !== noteIdToDelete);
                setNotes(updatedNotes);
            }

            handleCloseConfirmModal();
        }
    }

    const handleCloseConfirmModal = () => {
        setOpenConfirmModal(false);
        setNoteIdToDelete(null);
    }

    if (loading) return (
        <div className="w-full flex justify-center items-center h-screen">
            <div>Loading...</div>
        </div>
    );

    if (!notes.length)
        return (
            <div className="flex justify-center items-center h-full mt-5">
                <p>No notes found</p>
            </div>
        );

    return (
        <>
            <div className="max-w-lg mx-auto mt-5">
                <h1 className="text-2xl font-bold mb-4">Note list</h1>
                <ul className="divide-y divide-gray-200">
                    {notes.map((note: Note) => (
                        <li key={note.id} className="py-4">
                            <h2 className="text-lg font-semibold">{note.title}</h2>
                            <p className="text-gray-400 py-2">{note.content}</p>
                            <Link href={`/edit/${note.id}`} className="">
                                <span className="bg-green-500 hover:bg-green-700 text-white font-bold py-2 px-4 rounded-sm text-xs">
                                    Edit
                                </span>
                            </Link>
                            <button className="ml-4" onClick={handleOpenConfirmModal(note.id)}>
                                <span className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded-sm text-xs">
                                    Delete
                                </span>
                            </button>
                        </li>
                    ))}
                </ul>
            </div>
            <DeleteConfirmModal
                isOpen={openConfirmModal}
                onClose={handleCloseConfirmModal}
                onConfirm={handleDelete}
            />
        </>
    );
};

export default NoteList;
