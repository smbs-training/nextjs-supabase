import Link from 'next/link';
import React from 'react';
import AuthButton from '../AuthButton';
import { createClient } from '@/utils/supabase/server';

const Header: React.FC = () => {
    const canInitSupabaseClient = () => {
        // This function is just for the interactive tutorial.
        // Feel free to remove it once you have Supabase connected.
        try {
            createClient();
            return true;
        } catch (e) {
            return false;
        }
    };

    const isSupabaseConnected = canInitSupabaseClient();
    return (
        <header className="bg-gray-800 text-white py-4 px-6 w-full flex justify-between items-center p-3 text-sm font-bold">
            <Link href="/">
                <span className="text-lg font-bold">Notes</span>
            </Link>
            {isSupabaseConnected && <AuthButton />}
        </header>
    );
};

export default Header;