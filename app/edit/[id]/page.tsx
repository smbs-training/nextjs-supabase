'use client';
import React, { useEffect } from 'react';
import NoteForm from '@/components/noteForm/NoteForm';
import { useRouter } from 'next/navigation';
import { createClient } from '@/utils/supabase/client';

type EditNoteProps = {
    params: {
        id: number;
    }
};

type NoteData = {
    title: string;
    content: string;
};


const EditNotePage = (props: EditNoteProps) => {
    const { id: noteId } = props.params;
    const [loading, setLoading] = React.useState<boolean>(true);
    const [noteData, setNoteData] = React.useState<NoteData | undefined>(undefined);
    const { replace } = useRouter();
    const supabase = createClient();

    useEffect(() => {
        verifyUser();
        setLoading(true);
        getNote().then((data): void => {
            setNoteData(data);
            setLoading(false);
        });
    }, []);

    const verifyUser = async () => {
        const { data: { user } } = await supabase.auth.getUser();
        if (!user) return replace('/login');
    }

    const getNote = async (): Promise<NoteData> => {
        const response = await fetch(`/api/notes/${noteId}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            }
        });

        const { data } = await response.json();
        return data;
    }

    const handleSave = (title: string, content: string) => {
        fetch(`${origin}/api/notes/${noteId}`, {
            method: 'PATCH',
            body: JSON.stringify({ title, content }),
            headers: {
                'Content-Type': 'application/json',
            },
        }).then(() => {
            replace('/');
        }).catch((error) => {
            console.error('Error:', error);
        });
    };

    if (loading) return (
        <div className="w-full flex justify-center items-center h-screen">
            <div>Loading...</div>
        </div>
    );

    return (
        <NoteForm
            formTitle="Edit Note"
            noteData={noteData}
            onSave={handleSave}
        />
    );
};


export default EditNotePage;