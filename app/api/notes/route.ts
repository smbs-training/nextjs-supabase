import { createClient } from "@/utils/supabase/server";
import { NextRequest, NextResponse } from "next/server";

type PostPayload = {
    title: string;
    content: string;
    user_id: string;
};

export async function GET() {
    const supabase = createClient();

    const { data: { user } } = await supabase.auth.getUser();

    if (!user) return NextResponse.json({
        status: 401,
        error: 'Unauthorized'
    });

    const { data, error } = await supabase
        .from('note')
        .select()
        .eq('user_id', user?.id);

    if (error) {
        return NextResponse.error()
    }

    return NextResponse.json({ data });
}

export async function POST(req: NextRequest) {
    const supabase = createClient();
    const { title, content, user_id }: PostPayload = await req.json();

    if (!title || !user_id) {
        return NextResponse.json({
            status: 400,
            error: 'Title and user_id are required'
        });
    }

    const { data, error } = await supabase.from('note').insert({ title, content, user_id }).select();

    if (error) {
        return NextResponse.error()
    }

    return NextResponse.json({ data });
}
