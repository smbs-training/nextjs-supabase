import React from 'react';
import Header from '@/components/layout/Header';
import Footer from '@/components/layout/Footer';

type LayoutProps = {
    children: React.ReactNode;
};

const Layout: React.FC<LayoutProps> = ({ children }) => {
    return (
        <div className="flex flex-col min-h-screen bg-gray-700">
            <Header />
            <main className="flex-grow w-screen">{children}</main>
            <Footer />
        </div>
    );
};

export default Layout;