import React from 'react';

interface DeleteConfirmModalProps {
    isOpen: boolean;
    onClose: () => void;
    onConfirm: () => void;
}

const DeleteConfirmModal: React.FC<DeleteConfirmModalProps> = ({
    isOpen,
    onClose,
    onConfirm,
}) => {
    if (!isOpen) {
        return null;
    }

    return (
        <div className="fixed inset-0 flex items-center justify-center z-50">
            <div className="bg-white p-4 rounded shadow-lg">
                <h2 className="text-xl font-bold mb-4 text-black">Confirm Delete</h2>
                <p className="mb-4 text-black">Are you sure you want to delete this record?</p>
                <div className="flex justify-end">
                    <button
                        className="px-4 py-2 mr-2 text-white bg-red-500 rounded"
                        onClick={onConfirm}
                    >
                        Delete
                    </button>
                    <button
                        className="px-4 py-2 text-gray-500 bg-gray-200 rounded"
                        onClick={onClose}
                    >
                        Cancel
                    </button>
                </div>
            </div>
        </div>
    );
};

export default DeleteConfirmModal;